import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagenPipe } from './imagen.pipe';
import { ParesPipe } from './pares.pipe';
import { FiltroNoimagenPipe } from './filtro-noimagen.pipe';

@NgModule({
  declarations: [ImagenPipe, ParesPipe, FiltroNoimagenPipe],
  imports: [
    CommonModule
  ],
  exports:[ImagenPipe,ParesPipe,FiltroNoimagenPipe],

})
export class PipesModule { }
