import { Pipe, PipeTransform } from '@angular/core';

import {GLOBAL} from '../services/global';

@Pipe({
  name: 'imagen'
})


export class ImagenPipe implements PipeTransform {

  private URL=GLOBAL.imageURL;

  transform(img: string, size: string="w500"): any {

    if(!img){
        return "./assets/no-image-banner.jpg";
    }

    const urlImg=this.URL+size+img;
    return urlImg;
  }

}
