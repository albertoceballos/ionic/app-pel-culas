//filtro para no mostrar las películas que no tengan la imagen del backdrop_path
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroNoimagen'
})
export class FiltroNoimagenPipe implements PipeTransform {

  transform(peliculas: Array<any>, args?: any): any {

    return peliculas.filter(peli=>{
      return peli.backdrop_path;
    });
  }

}
