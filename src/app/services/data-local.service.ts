import { Injectable } from '@angular/core';
import { MovieDetails } from '../interfaces/movies';
//ionic Storage
import { Storage } from '@ionic/storage';

import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  public peliculas:MovieDetails[];
  public mensaje:string;
  constructor(private _storage:Storage,private _toastController:ToastController) {
    this.peliculas=[];
   }

  guardarPelicula(pelicula:MovieDetails){

    var existe:boolean=false;
    this.peliculas.forEach(peli => {
      if (peli.id==pelicula.id){
        existe=true;
      }
    });
    console.log(existe);
    if(existe){
      this.peliculas=this.peliculas.filter(peli=>{
        peli.id!=pelicula.id;
        this.mensaje='Película borrada de favoritos';
      });
    }else{
      this.peliculas.push(pelicula);
      this.mensaje='Película añadida a favoritos';
    }
    //guardar en el Storage
    this._storage.set('peliculas',this.peliculas);
    //presentar Toast
    this.presentToast(this.mensaje);
  }

  //toast
  async presentToast(message) {
    const toast = await this._toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  async cargarFavoritos(){
    var peliculas=await this._storage.get('peliculas');
    this.peliculas=peliculas || [];
    return this.peliculas;
  }

  async existePelicula(id){
    console.log(id);
    await this.cargarFavoritos();
    var existe=false;
    this.peliculas.forEach(element => {
      if(element.id==id){
        existe=true;
      }
    });
    console.log(existe);
    return existe;
  }
}
