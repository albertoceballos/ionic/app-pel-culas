import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';

//globales
import {GLOBAL} from '../services/global';

//interface
import {MovieResult, MovieDetails, CreditsResult,} from '../interfaces/movies';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private apiKey=GLOBAL.apiKey;
  private apiURL=GLOBAL.apiURL;
  private apiLanguage=GLOBAL.apiLanguage;
  private popularesPage:number;
  constructor(private _httpClient:HttpClient) {
    this.popularesPage=0;
   }

   getEstrenos(){
     var hoy=new Date();
     var ultimaDia=new Date(hoy.getFullYear(),hoy.getMonth()+1,0).getDate();
     var mes=hoy.getMonth()+1;

     var mesString:string;
     if(mes<10){
       mesString='0'+mes;
     }else{
       mesString=mes.toString();
     }

     var fechaInicio=hoy.getFullYear()+'-'+mesString+'-'+hoy.getDate();
     var fechaFin=hoy.getFullYear()+'-'+mesString+'-'+ultimaDia;

     return this._httpClient.get<MovieResult>(this.apiURL+'discover/movie?primary_release_date.gte='+fechaInicio+'&primary_release_date.lte='+fechaFin+'&'+this.apiKey+'&'+this.apiLanguage);
   }

   getPopulares(){
     this.popularesPage++;
     return this._httpClient.get<MovieResult>(this.apiURL+'discover/movie?sort_by=popularity.desc&'+this.apiKey+'&page='+this.popularesPage);
   }

   getDetallesPelicula(id){
    return this._httpClient.get<MovieDetails>(this.apiURL+'movie/'+id+'?'+this.apiKey+'&'+this.apiLanguage);
   }

   getActors(id){
     return this._httpClient.get<CreditsResult>(this.apiURL+'movie/'+id+'/credits?'+this.apiKey);
   }

   searchMovies(searchString,searchMoviesPage){
    return this._httpClient.get<MovieResult>(this.apiURL+'search/movie?'+this.apiKey+'&'+this.apiLanguage+'&query='+searchString+'&page='+searchMoviesPage);
   }

   //todos los géneros
   AllGenres():Observable<any>{
     return this._httpClient.get(this.apiURL+'genre/movie/list?api_key=1865f43a0549ca50d341dd9ab8b29f49&'+this.apiLanguage+'&include_image_language=es');
   }
}
