import { Component, OnInit } from '@angular/core';

//servicios
import {MoviesService} from '../services/movies.service';

//interfaces
import {Movie} from '../interfaces/movies';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  public peliculasEstreno:Array<Movie>;
  public peliculasPopulares:Array<Movie>;
  public slideOptions;
  constructor(private _movieService:MoviesService) {
    this.peliculasEstreno=[];
    this.peliculasPopulares=[];
   
  }

  ngOnInit(){
    this.getEstrenos();
    this.getPopulares();
  }


  getEstrenos(){
    this._movieService.getEstrenos().subscribe(
      response=>{
        this.peliculasEstreno=response.results;
      },
      error=>{
        console.log(error);
      }
    );
  }

  getPopulares(){
    this._movieService.getPopulares().subscribe(
      response=>{
        var tempArray=[...this.peliculasPopulares,...response.results]
        this.peliculasPopulares=tempArray;
      },
      error=>{
        console.log(error);
      }
    )
  }

  cargarMas(){
    console.log("CARGAR MÁS PULSADO");
    this.getPopulares();
  }



}
