import { Component } from '@angular/core';
import { MovieDetails, Genre } from '../interfaces/movies';
import { DataLocalService } from '../services/data-local.service';
import { MoviesService } from '../services/movies.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  public peliculas: MovieDetails[];
  public generos: Genre[];

  public favoritosGenero: Array<any>;
  constructor(private _dataLocalService: DataLocalService, private _movieService: MoviesService) { }

  
  async ionViewWillEnter(){
   
      this.peliculas = await this._dataLocalService.cargarFavoritos();
      console.log(this.peliculas);
  
      this._movieService.AllGenres().subscribe(
        response => {
          this.generos = response.genres;
          
          this.pelisPorGenero(this.generos,this.peliculas);
          console.log(this.favoritosGenero);
        },
        error => {
          console.log(error);
        }
      );
  }

  pelisPorGenero(generos: Genre[], peliculas: MovieDetails[]) {
    this.favoritosGenero = [];

    generos.forEach(genero => {
      this.favoritosGenero.push({
        genero: genero,
        pelis: peliculas.filter(peli => {
          return peli.genres.find(genre => genre.id == genero.id)
        })
      });
    });
  }
}
