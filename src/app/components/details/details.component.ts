import { Component, OnInit, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { MoviesService } from 'src/app/services/movies.service';
import { MovieDetails, Actors } from 'src/app/interfaces/movies';
import { GLOBAL } from 'src/app/services/global';
import { DataLocalService } from 'src/app/services/data-local.service';



@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {

  @Input() movieId:number;
  public details:MovieDetails;
  public actors:Actors[];
  public imageURL=GLOBAL.imageURL;
  public oculto:number;
  public slideOptions;
  public favIcon:string;
  constructor(private _movieService:MoviesService,
    private _dataLocalService:DataLocalService,
    private _modalController:ModalController) {
    this.favIcon="heart-empty";  
    this.oculto=150;
    this.slideOptions={
      slidesPerView: 3.2,
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows: true,
      },
    }
   }

  async ngOnInit() {
    var existe= await this._dataLocalService.existePelicula(this.movieId);
    console.log('Detalle component:',existe);
    (existe)?this.favIcon="heart-dislike":this.favIcon='heart-empty';

    this.getMovieDetails(this.movieId);
    this.getActors(this.movieId);
  }

  getMovieDetails(id){
    this._movieService.getDetallesPelicula(id).subscribe(
      response=>{
        this.details=response;
      },
      error=>{
        console.log(error);
      }
    );
  }

  getActors(id){
    this._movieService.getActors(id).subscribe(
      response=>{
        this.actors=response.cast;
      },
      error=>{
        console.log(error);
      }
    )
  }

  //cerrar modal
  volver(){
    this._modalController.dismiss();
  }

  //leer mas texto de la descripción de la película
  leerMas(){
    this.oculto=5000;
  }

  clickFavorite(){
    this._dataLocalService.guardarPelicula(this.details);
    if(this.favIcon=='heart-empty'){
      this.favIcon='heart-dislike';
    }else{
      this.favIcon='heart-empty';
    }
  }

}
