import { Component, OnInit, Input} from '@angular/core';
import { Movie } from 'src/app/interfaces/movies';

//Controlador del modal
import {ModalController} from '@ionic/angular'

//componente del modal
import {DetailsComponent} from '../details/details.component';


@Component({
  selector: 'app-slideshow-backdrop',
  templateUrl: './slideshow-backdrop.component.html',
  styleUrls: ['./slideshow-backdrop.component.scss'],
})
export class SlideshowBackdropComponent implements OnInit {

  @Input() movies:Movie[];
  public slideOptions;
  constructor(private modalController:ModalController) {
    this.movies=[];
    this.slideOptions={
      slidesPerView:1.2,
      freeMode:true,
    };
   }

  ngOnInit() {}

  async presentModal(id) {
    const modal = await this.modalController.create({
      component: DetailsComponent,
      componentProps: {
        'movieId': id,
      }
    });
    return await modal.present();
  }

}
