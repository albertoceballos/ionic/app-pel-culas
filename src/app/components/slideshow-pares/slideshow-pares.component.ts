import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import { Movie } from 'src/app/interfaces/movies';

//Controlador del modal
import {ModalController} from '@ionic/angular'

//componente del modal
import {DetailsComponent} from '../details/details.component';

@Component({
  selector: 'app-slideshow-pares',
  templateUrl: './slideshow-pares.component.html',
  styleUrls: ['./slideshow-pares.component.scss'],
})
export class SlideshowParesComponent implements OnInit {

  @Input() movies:Movie[];
  @Output() cargarMas=new EventEmitter();
  public slideOptions;
  constructor(private modalController:ModalController) {
    this.movies=[];
    this.slideOptions={
      slidesPerView:2.6,
      freeMode:true,
    };
  }
  ngOnInit() {}

  onClick(){
    this.cargarMas.emit();
  }

  async presentModal(id) {
    const modal = await this.modalController.create({
      component: DetailsComponent,
      componentProps: {
        'movieId': id,
      }
    });
    return await modal.present();
  }
}
