import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import {SlideshowBackdropComponent} from './slideshow-backdrop/slideshow-backdrop.component';
import { SlideshowPosterComponent } from './slideshow-poster/slideshow-poster.component';

import { PipesModule } from '../pipes/pipes.module';
import { SlideshowParesComponent } from './slideshow-pares/slideshow-pares.component';
import { DetailsComponent } from './details/details.component';





@NgModule({
  entryComponents:[DetailsComponent],
  declarations: [SlideshowBackdropComponent,SlideshowPosterComponent,SlideshowParesComponent,DetailsComponent],
  imports: [
    CommonModule,
    IonicModule,
    PipesModule,
  ],
  exports:[
    SlideshowBackdropComponent,
    SlideshowPosterComponent,
    SlideshowParesComponent,
    DetailsComponent,
  ]
})
export class ComponentsModule { }
