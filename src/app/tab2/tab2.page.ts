import { Component, ViewChild } from '@angular/core';
import { MoviesService } from '../services/movies.service';

//interfaces
import { MovieResult, Movie } from '../interfaces/movies';

import {DetailsComponent} from '../components/details/details.component';

//controlador de modal
import {ModalController, IonInfiniteScroll} from '@ionic/angular';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  @ViewChild(IonInfiniteScroll) infiniteScroll:IonInfiniteScroll;
  public busqueda:string;
  public movies:Movie[];
  public buscando:boolean;
  public page:number;
  public totalPages:number;
  public lastPage:boolean;
  public messageLast:string;
  public searchPage:number;
  constructor(private _movieService:MoviesService,private modalController:ModalController) {
    this.busqueda="";
    this.movies=[];
    this.buscando=false;
    this.lastPage=false;
    this.searchPage=1;
  }

  //al terminar de escribir la búsqueda
  onSearchChange(evento){
    this.buscando=true;
    this.infiniteScroll.disabled=false;
    this.busqueda=evento.detail.value;
    this.movies=[];
    this.searchPage=1;
    this.messageLast=null;
    this.lastPage=false;
    if(this.busqueda===""){
      this.buscando=false;
      return;
    }
    this.searchMovies(this.busqueda,this.searchPage);
    console.log(this.infiniteScroll.disabled.valueOf());
  }

  searchMovies(searchString,searchPage){

    
    this._movieService.searchMovies(searchString,searchPage).subscribe(
      response=>{
        this.page=response.page;
        this.totalPages=response.total_pages
        if(this.page==this.totalPages){
          this.lastPage=true;
        }
        this.movies.push(...response.results);
        this.buscando=false;
        console.log(response);
      },
      error=>{
        console.log(error);
      }
    );
  }

  async presentModal(id) {
    const modal = await this.modalController.create({
      component: DetailsComponent,
      componentProps: {
        'movieId': id,
      }
    });
    return await modal.present();
  }

  //cargar más datos con infinite-scroll
  loadData(evento){
      if(this.lastPage===false){
        evento.target.disabled = false;
        this.messageLast=null;
        this.searchPage++;
        this.searchMovies(this.busqueda,this.searchPage);
        evento.target.complete();
      }else{
        evento.target.disabled = true;
        this.messageLast="No hay mas resultados";
      }   
  }
}
